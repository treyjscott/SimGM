### PLOTS FOR TUTORIAL ###

# Fitness function
plot_fit_fun()
ggsave('site/images/fitness.jpg', width = 4, height = 4)

# Mutations

mut()
ggsave('site/images/mut.jpg', width = 4, height = 4)
# animated
animate(animate_mut(), width = 4, height = 4, units = 'in', res = 300)
anim_save('site/images/mut.gif', width = 4, height = 4)

# Runs simulation
FGM_t5000 <- GM(n = 2, t = 5000, mal = 0.25)

# Plot fitness
plot_fitness(FGM_t5000)
ggsave('site/images/plot_fitness.jpg', width = 5, height = 3)
# Plots output
plot_2n(FGM_t5000)
ggsave('site/images/plot_2n.jpg', width = 4, height = 4)

# Plot animation
animate(animate_plot_2n(FGM_t5000), width = 4, height = 4, units = 'in', res = 300)
# Broken for some reason


### ABiotic
abiotic_FGM <- envGM(n = 2, t = 2500, mal = 0.25)
# Plot
plot_2n(abiotic_FGM)
ggsave('site/images/plot_2nab.jpg', width = 4, height = 4)

# Conflict
cFGMlist <- conflictGM(t = 5000, n1 = 1, n2 = 1)
# Save party 1 object
cFGM1 <- cFGMlist[[1]]
# Save party 2 object
cFGM2 <- cFGMlist[[2]]
plot_jt(cFGMlist)
ggsave('site/images/jt.jpg', width = 5, height = 3)

# Replication
# Replicate with varying population size, maladaptation, mutation size, and selection strength
summary.out <- replicate_GM(sims = 10, N = c(10, 500, 1000), mal = c(0.1, 0.2), m = c(0.05, 0.1), a = c(0.5,  0.75), t = 1000)
# Plot fitness results
plot_reps(summary.out, y = 'Fitness')
ggsave('site/images/repFGM.jpg', width = 4, height = 4)

# Replicate models over time
temp.mods <- replicate_all_temp(sims = 10, n = 1, t = 1000)
ggplot(temp.mods, aes(Time, Fitness, color = Model)) + geom_line() + scale_color_manual(values = pal2)
ggsave('site/images/temp.jpg', width = 5, height = 3)

# Replicate different models
mod.summary <- replicate_all(sims = 10, n =1, N = c(10, 500, 5000), t = 1000)
plot_reps(mod.summary, y = 'Fitness')
ggsave('site/images/mods.jpg', width = 5, height = 5)

# Replicate custom function
summary.out2 <- replicate_GM(t = 1000, fun = list('time to fitness' = function(x) {find_time_to_fitness(x, fitness = 0.95)}))
plot_reps(summary.out2, y = 'time to fitness', type = 'box')
ggsave('site/images/custom_reps.jpg', width = 5, height = 5)
