
![](site/images/SimGM.jpg)
## Description 

SimGM contains functions for simulating and analyzing Fisher's geometric model in different environments. Environments include (1) a stable optimum, a (2) randomly moving optimum, (3) a directionally (correlated) moving optimum, (4) mutualism, (5) conflict over joint phenotypes, and (6) victim-exploiter conflict. SimGM also contains functions for visualizing simulation output and basic elements of the geometric model (eg. mutations) for educational purposes. 

A tutorial for students can be found at https://treyjscott.gitlab.io/SimGM/.

## Installation

This package must be installed using devtools and from gitlab.

```
# Install devtools
install.packages("devtools")

# Install SimGM
devtools::install_gitlab("treyjscott/SimGM")
```

## Usage

```
### BASICS OF SimGM ###

# Load the package
library(SimGM)

# Visualize a random mutation
mut()

# Run a single simulation of the standard geometric model with a stable optimum for 500 timepoints
FGMobject <- FGM(t = 500)

# Plot fitness
plot_fitness(FGMobject)

# Run a simulation with two traits
FGMobject <- FGM(n = 2, t = 500)

# Plot adaptive walk in 2 dimensions
plot_2n(FGMobject)

# Simulate conflict over joint phenotype
cFGMlist <- conflictFGM(n = 1, t = 500)

# Get party 1 and party 2 objects
party1FGMobject <- cFGMlist[[1]]
party2FGMobject <- cFGMlist[[2]]

# Plot joint phenotype over time
plot_jt(cFGMlist)

# Simulate victim-exploiter and trait matching mutualism
veFGMlist <- veFGM(n = 1, t = 500) # Victim is indexed 1 and expoiter is indexed 2
mFGMlist <- mutualismFGM(n = 1, t = 500)

# Simulate adaptation to changing environment with random change
envFGMobject <- envFGM(n = 2, t = 500)
plot_2n(envFGMobject)

# Use exploiter fixations (second element of list) as magnitude for environmental change
envFGMobject2 <- envFGM(change = veFGMlist[[2]])

### REPLICATE AND PLOT SIMULATIONS ###

# Replicate standard geometric model simulation and get averages for n = 1, 5, and 10
FGMaverages <- replicate_FGM(sims = 10, t = 500, n = c(1,5,10))

# Plot average values (defulat plots mean of fitness with standard errors)
plot_reps(FGMaverages)

# Plot number of fixations as boxplots (can also plot points with variance or violin plots)
plot_reps(FGMaverages, y = Fixations, type = 'box')

# Replicate all models (standard, randomly changing optimum, directionally changing optimum, joitn phenotype conflict, victim-exploiter conflict)
Modelaverages <- replicate_all(sims = 25, t = 1000, n = c(1, 5, 10))

# Plot the results (plot_reps flexibly plots the outcome of all replicate functions)
plot_reps(Modelaverages)

### REPLICATE SIMULATIONS WITH RANDOMLY DRAWN PARAMETERS ###

# Simulate replicates (parameters in vectors give intervals for random draws)
randomReps <- replicate_all_random(sims = 50, n = c(1, 10), a = c(0.1, 0.9)) # draws random n from 1 to 10 and a from 0.1 to 0.9

# Random replicates can be anlayzed using Lasso regression with plot_effects function
LassoResults <- plot_effects(randomReps)

#Plot the output (first element of returned list)
LassoResults[[1]] # Plots relative effects from Lasso (effect divided by sum of effects after removing the intercept)

### GENERATE HYBRID CROSSES ###

# Simulate two independent populations adapting to randomly changing environments
RandomChange1 <- envFGM(t = 500, n = 2)
RandomChange2 <- envFGM(t = 500, n = 2)

# Plot adaptive walks
plot_2n(RandomChange1)
plot_2n(RandomChange2)

# Create hybrid crosses
crosses <- generate_crosses(RandomChange1, RandomChange2)
```

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Citation
Scott, T. J. (2022). SimGM (Version 1.0). Zenodo. https://doi.org/10.5281/zenodo.7236727
