
#' Simulates evolutionary conflict over one or more joint phenotypes
#'
#' @param n1 number of traits (complexity) for party 1
#' @param n2 number of traits (complexity) for party 2
#' @param v number of joint traits
#' @param m1 average mutation size for party 1
#' @param m2 average mutation size for party 2
#' @param mal numeric amount of maladaptation for both parties (ranges from 0 to 1)
#' @param a1 numeric value that scales the strength of stabilizing selection for party 1
#' @param a2 numeric value that scales the strength of stabilizing selection party 2
#' @param N1 integer population size for party 1
#' @param N2 integer population size for party 2
#' @param distr1 string distribution for random mutations for party 1 (default is 'normal';'exponential' or 'uniform' are other options)
#' @param distr2 string distribution for random mutations for party 2 (default is 'normal';'exponential' or 'uniform' are other options)
#' @param ploidy1 integer value of ploidy (haploid = 1 or diploid = 2)
#' @param ploidy2 integer value of ploidy (haploid = 1 or diploid = 2)
#' @param t integer number of time points to simulate
#' @param max_fixed1 integer number of fixations to simulate for victim
#' @param max_fixed2 integer number of fixations to simulate for exploiter (can only choose one to count fixations for)
#'
#' @return GeometricModel object
#'
#' @details
#' Joint phenotypes are shared between two parties. Conflict occurs if parties have different optimal values. Evolution in this model occurs as in other instances of the geometric model, but with each party randomly taking turns to draw mutations.
#'
#'
#' @examples
#' ### Simulate conflict and assign output to variable
#' cFGMlist <- conflictFGM()
#' ### Save party 1 object
#' cFGM1 <- cFGMlist[[1]]
#' ### Save party 2 object
#' cFGM2 <- cFGMlist[[2]]
#'
#' @references
#' Scott, T. J., and D. C. Queller. 2019. Long‐term evolutionary conflict, Sisyphean arms races, and power in Fisher’s geometric model. Ecology and Evolution 9:11243–11253.
#'
#' @export
#'

conflictGM <- function(n1 = 10, n2 = n1,
                        v = n1,
                        m1 = 0.1, m2 = m1,
                        mal = 0.1,
                        a1 = 0.5, a2 = a1,
                        N1 = 1000, N2 = N1,
                        distr1 = 1000, distr2 = distr1,
                        ploidy1 = 1, ploidy2 = ploidy1,
                        t = 100, max_fixed1 = NULL, max_fixed2 = NULL) {
  # Check parameters
  if (v > n1 || v > n2) {
    stop('Number of joint traits cannot exceed numer of traits!')
  }
  ### data frame for tracking simulation
  data1 <- data.frame(matrix(ncol = 6+n1, nrow = 2*t+1))
  data2 <- data.frame(matrix(ncol = 6+n2, nrow = 2*t+1))
  if (v != n1) {
    names(data1) <- c('Time','s','Mutation Size', 'Distance', 'Fitness', 'Fixed',paste('Joint Trait',1:v), paste('Trait', (v+1):n1))
  } else {
    names(data1) <- c('Time','s','Mutation Size', 'Distance', 'Fitness', 'Fixed',paste('Joint Trait',1:v))
  }
  if (v != n2) {
    names(data2) <- c('Time','s','Mutation Size', 'Distance', 'Fitness', 'Fixed',paste('Joint Trait',1:v), paste('Trait', (v+1):n2))
  } else {
    names(data2) <- c('Time','s','Mutation Size', 'Distance', 'Fitness', 'Fixed',paste('Joint Trait',1:v))
  }
  #data1$Time <- data2$Time <-  0:t
  # Create dataframe for fixed mutations
  fixations1 <- data1[1:(t+1), -1*3:5]
  fixations1$Time <-  0:t
  fixations1$Fixed <- 0
  fixations1$past_s <- fixations1$e <- NA
  fixations2 <- data2[1:(t+1), -1*3:5]
  fixations2$Time <-  0:t
  fixations2$Fixed <- 0
  fixations2$past_s <- fixations2$e <- NA
  # start traits at the origin
  pos1 <- rep(0, n1)
  pos2 <- rep(0, n2)
  # calculate optimum values for joint traits by creating a vector and reversing it
  #trait.values1 <- create_vector(sqrt((-log(1-mal)/a1)),v)
  #trait.values2 <- create_vector(sqrt((-log(1-mal)/a2)),v)
  #op1 <- c(trait.values1,rep(0,n1-v))
  #op2 <- c(-(trait.values1/abs(trait.values1))*abs(trait.values2),rep(0,n2-v))
  op1 <- c(rep(sqrt((-log(1-mal)/a1)/v),v),rep(0,n1-v))
  op2 <- c(rep(-sqrt((-log(1-mal)/a2)/v),v),rep(0,n2-v))
  fixed1 <- FALSE
  fixed2 <- FALSE
  data1[1, ] <- c(0,0,0,euclidean_distance(pos1), gaussian_fitness(pos1, op1, a1),fixed1, pos1)
  data2[1, ] <- c(0,0,0,euclidean_distance(pos2), gaussian_fitness(pos2, op2, a2),fixed2, pos2)
  # loop through timepoints
  for (i in 1:t) {
    index <- 1
    #print(which(is.na(data1$Time)))
    if (runif(1) >= 0.5) {
      ### Party 1 mutates first
      mut1 <- muttt(distr = distr1, m = m1, n = n1)
      h1 <- dominance_coefficient(pos1, mut1, op1, a1)
      new_pos1 <- pos1 + mut1
      mutant_fitness1 <- gaussian_fitness(new_pos1,op1,a1)
      current_fitness1 <- gaussian_fitness(pos1,op1,a1)
      s_c1 <- selection_coefficient(mutant_fitness1,current_fitness1)
      fixation.prob1 <- fixation_probability(mutant_fitness1, current_fitness1, N1, ploidy1, h1)
      # Update positions if mutations fix
      if (fixation.prob1 >= runif(1)) {
        pos1 <- new_pos1
        pos2[1:v] <- pos1[1:v]
        fixed1 <- TRUE
        current_fitness1 <- mutant_fitness1
        fixations1[i+1, ] <- c(i, s_c1, fixed1, mut1,NA, NA)
        if (nrow(fixations1[fixations1$Fixed == 1, ]) > 1) {
          fixed_i <- fixations1[fixations1$Fixed == 1, ]$Time
          prev_mut_i <- fixed_i[which(fixed_i == i)-1]
          p00_i <- prev_mut_i-1
          #print(fixations2[fixations2$Fixed == 1 & fixations2$Time >= p00_i, ])
          if (nrow(fixations2[fixations2$Fixed == 1 & fixations2$Time > p00_i, ]) == 1) {
            prev_mut_ant <- c(fixations2[fixations2$Fixed == 1 & fixations2$Time > p00_i, 4:(3+v)],rep(0,n1-v))
          } else if (nrow(fixations2[fixations2$Fixed == 1 & fixations2$Time > p00_i, ]) > 1) {
            prev_mut_ant <- c(colSums(fixations2[fixations2$Fixed == 1 & fixations2$Time > p00_i, 4:(3+v), drop=FALSE]),rep(0,n1-v))
          } else {
            prev_mut_ant <- rep(0,n1)
          }
          anc_phen <- data1[data1$Time == p00_i & !is.na(data1$Time), 7:(6+n1), drop = FALSE]
          jt10 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant+mut1
          jt01 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant+fixations1[fixations1$Time == prev_mut_i, 4:(3+n1)]
          jt00 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant
          fixations1$e[i+1] <- calculate_epistasis(w11 = current_fitness1,
                                                   w10 = gaussian_fitness(jt10, op1, a1),
                                                   w01 = gaussian_fitness(jt01, op1, a1),
                                                   w00 = gaussian_fitness(jt00, op1, a1))
        }
        data1[min(which(is.na(data1$Time))), ] <- c(i,s_c1,euclidean_distance(mut1),euclidean_distance(pos1, op1), gaussian_fitness(pos1, op1, a1),fixed1, pos1)
        data2[min(which(is.na(data2$Time))), ] <- c(i,NA,NA,euclidean_distance(pos2, op2), gaussian_fitness(pos2, op2, a2),fixed2, pos2)
        fixed1 <- FALSE
        fixed2 <- FALSE
      } else {
        data1[min(which(is.na(data1$Time))), ] <- c(i,s_c1,euclidean_distance(mut1),euclidean_distance(pos1, op1), gaussian_fitness(pos1, op1, a1),fixed1, pos1)
      }
      ### Party 2 mutates
      mut2 <- muttt(distr = distr2, m = m2, n = n2)
      h2 <- dominance_coefficient(pos2, mut2, op2, a2)
      new_pos2 <- pos2 + mut2
      mutant_fitness2 <- gaussian_fitness(new_pos2,op2,a2)
      current_fitness2 <- gaussian_fitness(pos2,op2,a2)
      s_c2 <- selection_coefficient(mutant_fitness2,current_fitness2)
      fixation.prob2 <- fixation_probability(mutant_fitness2, current_fitness2, N2, ploidy2, h2)
      # Update positions if mutations fix

      if (fixation.prob2 >= runif(1)) {
        pos2 <- new_pos2
        pos1[1:v] <- pos2[1:v]
        fixed2 <- TRUE
        current_fitness2 <- mutant_fitness2
        fixations2[i+1, ] <- c(i, s_c2, fixed2, mut2,NA, NA)
        if (nrow(fixations2[fixations2$Fixed == 1, ]) > 1) {
          fixed_i <- fixations2[fixations2$Fixed == 1, ]$Time
          prev_mut_i <- fixed_i[which(fixed_i == i)-1]
          p00_i <- prev_mut_i-1
          if (nrow(fixations1[fixations1$Fixed == 1 & fixations1$Time > p00_i, ]) == 1) {
            prev_mut_ant <- c(fixations1[fixations1$Fixed == 1 & fixations1$Time > p00_i, 4:(3+v)],rep(0,n2-v))
          } else if (nrow(fixations1[fixations1$Fixed == 1 & fixations1$Time > p00_i, ]) >= 1) {
            prev_mut_ant <- c(colSums(fixations1[fixations1$Fixed == 1 & fixations1$Time > p00_i, 4:(3+v), drop=FALSE]),rep(0,n2-v))
          } else {
            prev_mut_ant <- rep(0,n2)
          }
          anc_phen <- data2[data2$Time == p00_i & !is.na(data2$Time), 7:(6+n2), drop = FALSE]
          jt10 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant+mut2
          jt01 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant+fixations2[fixations2$Time == prev_mut_i, 4:(3+n2)]
          jt00 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant
          fixations2$e[i+1] <- calculate_epistasis(w11 = current_fitness2,
                                                   w10 = gaussian_fitness(jt10, op2, a2),
                                                   w01 = gaussian_fitness(jt01, op2, a2),
                                                   w00 = gaussian_fitness(jt00, op2, a2))
        }
        data1[min(which(is.na(data1$Time))), ] <- c(i,NA,NA,euclidean_distance(pos1, op1), gaussian_fitness(pos1, op1, a1),fixed1, pos1)
        data2[min(which(is.na(data2$Time))), ] <- c(i,s_c2,euclidean_distance(mut2),euclidean_distance(pos2, op2), gaussian_fitness(pos2, op2, a2),fixed2, pos2)
        fixed1 <- FALSE
        fixed2 <- FALSE
      } else {
        data2[min(which(is.na(data2$Time))), ] <- c(i,s_c2,euclidean_distance(mut2),euclidean_distance(pos2, op2), gaussian_fitness(pos2, op2, a2),fixed2, pos2)
      }
    } else {
      # Party 2 mutates first
      mut2 <- muttt(distr = distr2, m = m2, n = n2)
      h2 <- dominance_coefficient(pos2, mut2, op2, a2)
      new_pos2 <- pos2 + mut2
      mutant_fitness2 <- gaussian_fitness(new_pos2,op2,a2)
      current_fitness2 <- gaussian_fitness(pos2,op2,a2)
      s_c2 <- selection_coefficient(mutant_fitness2,current_fitness2)
      fixation.prob2 <- fixation_probability(mutant_fitness2, current_fitness2, N2, ploidy2, h2)
      # Update positions if mutations fix
      if (fixation.prob2 >= runif(1)) {
        pos2 <- new_pos2
        pos1[1:v] <- pos2[1:v]
        fixed2 <- TRUE
        current_fitness2 <- mutant_fitness2
        fixations2[i+1, ] <- c(i, s_c2, fixed2, mut2,NA, NA)
        if (nrow(fixations2[fixations2$Fixed == 1, ]) > 1) {
          fixed_i <- fixations2[fixations2$Fixed == 1, ]$Time
          prev_mut_i <- fixed_i[which(fixed_i == i)-1]
          p00_i <- prev_mut_i-1
          if (nrow(fixations1[fixations1$Fixed == 1 & fixations1$Time > p00_i, ]) == 1) {
            prev_mut_ant <- c(fixations1[fixations1$Fixed == 1 & fixations1$Time > p00_i, 4:(3+v)],rep(0,n2-v))
          } else if (nrow(fixations1[fixations1$Fixed == 1 & fixations1$Time > p00_i, ]) > 1) {
            prev_mut_ant <- c(colSums(fixations1[fixations1$Fixed == 1 & fixations1$Time > p00_i, 4:(3+v), drop=FALSE]),rep(0,n2-v))
          } else {
            prev_mut_ant <- rep(0,n2)
          }
          anc_phen <- data2[data2$Time == p00_i & !is.na(data2$Time), 7:(6+n2), drop = FALSE]
          jt10 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant+mut2
          jt01 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant+fixations2[fixations2$Time == prev_mut_i, 4:(3+n2)]
          jt00 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant
          fixations2$e[i+1] <- calculate_epistasis(w11 = current_fitness2,
                                                   w10 = gaussian_fitness(jt10, op2, a2),
                                                   w01 = gaussian_fitness(jt01, op2, a2),
                                                   w00 = gaussian_fitness(jt00, op2, a2))
        }
        data1[min(which(is.na(data1$Time))), ] <- c(i,NA,NA,euclidean_distance(pos1, op1), gaussian_fitness(pos1, op1, a1),fixed1, pos1)
        data2[min(which(is.na(data2$Time))), ] <- c(i,s_c2,euclidean_distance(mut2),euclidean_distance(pos2, op2), gaussian_fitness(pos2, op2, a2),fixed2, pos2)
        fixed1 <- FALSE
        fixed2 <- FALSE
      } else {
        data2[min(which(is.na(data2$Time))), ] <- c(i,s_c2,euclidean_distance(mut2),euclidean_distance(pos2, op2), gaussian_fitness(pos2, op2, a2),fixed2, pos2)
      }
      # Party 1 mutates
      mut1 <- muttt(distr = distr1, m = m1, n = n1)
      h1 <- dominance_coefficient(pos1, mut1, op1, a1)
      new_pos1 <- pos1 + mut1
      mutant_fitness1 <- gaussian_fitness(new_pos1,op1,a1)
      current_fitness1 <- gaussian_fitness(pos1,op1,a1)
      s_c1 <- selection_coefficient(mutant_fitness1,current_fitness1)
      fixation.prob1 <- fixation_probability(mutant_fitness1, current_fitness1, N1, ploidy1, h1)
      # Update positions if mutations fix
      if (fixation.prob1 >= runif(1)) {
        pos1 <- new_pos1
        pos2[1:v] <- pos1[1:v]
        fixed1 <- TRUE
        current_fitness1 <- mutant_fitness1
        fixations1[i+1, ] <- c(i, s_c1, fixed1, mut1,NA, NA)
        if (nrow(fixations1[fixations1$Fixed == 1, ]) > 1) {
          fixed_i <- fixations1[fixations1$Fixed == 1, ]$Time
          prev_mut_i <- fixed_i[which(fixed_i == i)-1]
          p00_i <- prev_mut_i-1
          if (nrow(fixations2[fixations2$Fixed == 1 & fixations2$Time > p00_i, ]) == 1) {
            prev_mut_ant <- c(fixations2[fixations2$Fixed == 1 & fixations2$Time > p00_i, 4:(3+v)],rep(0,n1-v))
          } else if (nrow(fixations2[fixations2$Fixed == 1 & fixations2$Time > p00_i, ]) > 1) {
            prev_mut_ant <- c(colSums(fixations2[fixations2$Fixed == 1 & fixations2$Time > p00_i, 4:(3+v), drop=FALSE]),rep(0,n1-v))
          } else {
            prev_mut_ant <- rep(0,n1)
          }
          anc_phen <- data1[data1$Time == p00_i & !is.na(data1$Time), 7:(6+n1), drop = FALSE]
          jt10 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant+mut1
          jt01 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant+fixations1[fixations1$Time == prev_mut_i, 4:(3+n1)]
          jt00 <- anc_phen[nrow(anc_phen), ]+prev_mut_ant
          fixations1$e[i+1] <- calculate_epistasis(w11 = current_fitness1,
                                                   w10 = gaussian_fitness(jt10, op1, a1),
                                                   w01 = gaussian_fitness(jt01, op1, a1),
                                                   w00 = gaussian_fitness(jt00, op1, a1))
          #w00 = data1[data1$Time == p00_i & !is.na(data1$s), ]$Fitness)
        }
        data1[min(which(is.na(data1$Time))), ] <- c(i,s_c1,euclidean_distance(mut1),euclidean_distance(pos1, op1), gaussian_fitness(pos1, op1, a1),fixed1, pos1)
        data2[min(which(is.na(data2$Time))), ] <- c(i,NA,NA,euclidean_distance(pos2, op2), gaussian_fitness(pos2, op2, a2),fixed2, pos2)
        fixed1 <- FALSE
        fixed2 <- FALSE
      } else {
        data1[min(which(is.na(data1$Time))), ] <- c(i,s_c1,euclidean_distance(mut1),euclidean_distance(pos1, op1), gaussian_fitness(pos1, op1, a1),fixed1, pos1)
      }
    }
    if (typeof(max_fixed1) == "double") {
      if (sum(data1$Fixed, na.rm = TRUE) >= max_fixed1) {
        print('Reached maximum number of fixations')
        break
      }
    }
    if (typeof(max_fixed2) == "double") {
      if (sum(data2$Fixed, na.rm = TRUE) >= max_fixed2) {
        print('Reached maximum number of fixations')
        break
      }
    }
    #data1[i+1, ] <- c(i,s_c1,euclidean_distance(mut1),euclidean_distance(pos1, op1), gaussian_fitness(pos1, op1, a1),fixed1, pos1)
    #data2[i+1, ] <- c(i,s_c2,euclidean_distance(mut2),euclidean_distance(pos2, op2), gaussian_fitness(pos2, op2, a2),fixed2, pos2)
    #fixed1 <- FALSE
    #fixed2 <- FALSE
  }
  # Remove excess NAs
  data1 <- data1[1:(min(which(is.na(data1$Time)))-1), ]
  data2 <- data2[1:(min(which(is.na(data2$Time)))-1), ]
  # Create and return GeometricModel Objects
  Party1 <- list(data = data1, fixations = fixations1[!is.na(fixations1$s), ], optimum = op1, n = n1, v = v, m = m1, mal = mal, a = a1, N = N1, distr = distr1, ploidy = ploidy1, t = t)
  Party2 <- list(data = data2, fixations = fixations2[!is.na(fixations2$s), ], optimum = op2, n = n2, v = v, m = m2, mal = mal, a = a2, N = N2, distr = distr2, ploidy = ploidy2, t = t)
  class(Party1) <- 'GeometricModel'
  class(Party2) <- 'GeometricModel'
  return(list('Party 1' = Party1, 'Party 2' = Party2))
}
